<?php

/*
 * Code lifted from "LaunchPad DispatchMan" as supplied by TFS
*
* Code debugged to remove long list of issues with processing XML data
*
* A J Monaghan
* Monaghan Consultants Ltd
* 14th October 2013
*
*/

class MonaghanConsultants_TFSDispatch_Helper_Writer extends Mage_Core_Helper_Abstract {


    protected $_data = null;

    # accepts 1 of 1 args
    # argument $data must be associative array
    # returns $this
    public function setData( $data ) {
        $this->_data = $data;
        return $this;
    }

    # public function getData
    # accepts 0 of 0 args
    # uses $this->is_assoc
    # returns $this->_data
    public function getData() {
        return $this->_data;
    }

    public function baseDataSpec() {
        return array(
        'H' => array('Header'=>'123'),
        'D' => array(
                array(
                        'A'=> 1,
                        'B'=> 2
                ),
                array(
                        'A'=> 1,
                        'B'=> 2
                ),
           )
        );

    }

    public function writeConfig( $filename ) {
        $baseDir = getcwd().DS.'var';
        $exportDir = $baseDir.DS.'export'.DS.'mkp';
        $outFile = $exportDir.DS.$filename;
        $remoteDir =  'OptimiserIn';
        $remoteFile = $remoteDir.DS.$filename;

        $sftp = 0;
        $port = 21;
        $timeout = 100;

        $host = Mage::getStoreConfig('tfsdispatch/tfsdispatch/ftp_host');
        $username =  Mage::getStoreConfig('tfsdispatch/tfsdispatch/ftp_username');
        $password = Mage::getStoreConfig('tfsdispatch/tfsdispatch/ftp_password');

//         Mage::log("Out File: $outFile");
//         Mage::log("Remote File: $remoteFile");
//         Mage::log(array(
//         		'host'      => $host,
//         		'user'      => $username,
//         		'username'  => $username,
//         		'password'  => $password,
//         		'port'  	=> $port,
//         		'file_mode' => FTP_ASCII,
//         		'passive' 	=> true,
//         		'timeout'   => $timeout,
//         ));
        try {
            $data = (string) MonaghanConsultants_TFSDispatch_Helper_Array2XML
                        ::createXML('Root', $this->getData())->saveXML();

            $remote = new Varien_Io_Ftp(); //$sftp ? new Varien_Io_Sftp() : new Varien_Io_Ftp();
            $remote->open(
                array(
                    'host'      => $host,
                    'user'      => $username,
                    'username'  => $username,
                    'password'  => $password,
                    'port'  	=> $port,
                    'file_mode' => FTP_ASCII,
                    'passive' 	=> true,
                    'timeout'   => $timeout,
                )
            );
            $remote->write($remoteFile, $data);

        } catch(Exception $ex) {
        	Mage::log("Remote File Error", null, 'tfsdispatch.log');
        	Mage::log($ex->getMessage(), null, 'tfsdispatch.log');
        }
    }
    public function is_assoc($arr) {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }

}