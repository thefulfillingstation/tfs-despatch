<?php

/*
 * Code lifted from "LaunchPad DispatchMan" as supplied by TFS
*
* Code debugged to remove long list of issues with processing XML data
*
* A J Monaghan
* Monaghan Consultants Ltd
* 14th October 2013
*
*/

class MonaghanConsultants_TFSDispatch_Helper_Reader extends Mage_Core_Helper_Abstract {


	protected $_data = null;

	# accepts 1 of 1 args
	# argument $data must be associative array
	# returns $this
	public function setData( $data ) {
		$this->_data = $data;
		return $this;
	}

	# public function getData
	# accepts 0 of 0 args
	# uses $this->is_assoc
	# returns $this->_data
	public function getData() {
		return $this->_data;
	}

	public function baseDataSpec() {
		return array(
				'H' => array('Header'=>'123'),
				'D' => array(
						array(
								'A'=> 1,
								'B'=> 2
						),
						array(
								'A'=> 1,
								'B'=> 2
						),
				)
		);

	}

	public function readConfig( ) {
		$baseDir = getcwd().DS.'var';
		$importDir = $baseDir.DS.'import'.DS.'mkp';
		$remoteDir =  'OptimiserOut';

		$sftp = 0;
		$port = 21;
		$timeout = 100;

		$host = Mage::getStoreConfig('tfsdispatch/tfsdispatch/ftp_host');
		$username =  Mage::getStoreConfig('tfsdispatch/tfsdispatch/ftp_username');
		$password = Mage::getStoreConfig('tfsdispatch/tfsdispatch/ftp_password');

		try {
			$io = new Varien_Io_File();
			$remote = $sftp ? new Varien_Io_Sftp() : new Varien_Io_Ftp();
			$remote->open(
					array(
							'host'      => $host,
							'user'      => $username,
							'username'  => $username,
							'password'  => $password,
							'port'  => $port,
							'file_mode' => FTP_ASCII,
							'passive' => true,
							'timeout'   => $timeout,
					)
			);
			$remote->cd($remoteDir);
			// file list
			$ls = $remote->ls();

			foreach($ls as $l)
			{
				$archive = false;
				try
				{ // inside the loop so loop doesn't break
					Mage::log('Filename: '. $l['text'], null, 'tfsdispatch.log');
					if($l['text'] == "." || $l['text'] == ".." || preg_match("/archive/i",$l['id']) == 1 ) continue;
					# archive
					$data = $remote->read($l['id']);
					$outFile = $importDir.DS.$l['text'];
					if($io->mkdir($importDir)) {
						throw new Exception("Permissions error. Please check your application permissions to create the directory '{$importDir}'.");
					} else {
						$io->write($outFile, $data);
					}

					$inboundHelper = new MonaghanConsultants_TFSDispatch_Helper_Inbound();
					$inboundHelper->setData($data);
					if (stristr($l['text'], 'desp') !== false)  // is it a dispatch
					{
						if (Mage::getStoreConfig('tfsdispatch/tfsdispatch/process_shipping_notifications'))
						{
							Mage::log('Dispatch: '. $l['text'], null, 'tfsdispatch.log');
							$archive = $inboundHelper->process();
						}
					}
					if (stristr($l['text'], 'stock') !== false)  // is it a stocklevel
					{
						if (Mage::getStoreConfig('tfsdispatch/tfsdispatch/process_stock_notifications'))
						{
							Mage::log('Stock: '. $l['text'], null, 'tfsdispatch.log');
							$archive = $inboundHelper->stocklevel();
						}
					}
					// Make sure we don't touch a file that we're disabled for.
					if ($archive)
					{
						if (!$remote->mv($l['id'], "./Archive/{$l['text']}"))
						{ 
							Mage::log("There was an error moving the remote file {$l['id']}", null, 'tfsdispatch.log');
						}
					}
				}
				catch (Exception $ex)
				{
					Mage::log($ex->getMessage(), null, 'tfsdispatch.log');
				}
			}
		} catch(Exception $ex) {
			Mage::log($ex->getMessage(), null, 'tfsdispatch.log');
		}
	}
}