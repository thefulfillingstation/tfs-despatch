<?php

/*
 * Code lifted from "LaunchPad DispatchMan" as supplied by TFS
*
* Code debugged to remove long list of issues with processing XML data
*
* A J Monaghan
* Monaghan Consultants Ltd
* 14th October 2013
*
*/


class MonaghanConsultants_TFSDispatch_Helper_Inbound extends Mage_Core_Helper_Abstract {
	protected $_data = null;

	public function setData($data) {
		$this->_data = $data;
		return $this;
	}
	public function getData() {
		return $this->_data;
	}
	public function process() {

		# REMOVED THE USE OF XTENTO

		// despatch confirmation by default
		// TODO: receipt confirmation and stock level export
		$xmlDataArray = $this->xml2array($this->getData());
		//    Mage::log($xmlDataArray);
		$headKey = isset($xmlDataArray['Root']) ? 'Root' : (isset($xmlDataArray['root']) ? 'root' :  'ROOT');
		if(!isset($xmlDataArray[$headKey])) 
		{
			Mage::log("Invalid file format!" , null, 'tfsdispatch.log');
			Mage::log($this->_data , null, 'tfsdispatch.log');
			return false;
		}

		$xmlDataArrayHeader = $xmlDataArray[$headKey]['H'];
		if (isset($xmlDataArray[$headKey]['D']))
		{
			$xmlDataArrayDataSet = isset($xmlDataArray[$headKey]['D']['Product']) ? array($xmlDataArray[$headKey]['D']) :  $xmlDataArray[$headKey]['D'];
		}
		else
		{
			$xmlDataArrayDataSet = isset($xmlDataArray[$headKey]['L']['Product']) ? array($xmlDataArray[$headKey]['L']) :  $xmlDataArray[$headKey]['L'];
		}

		$shippedOrders = array();
		foreach($xmlDataArrayDataSet as $dataItem) {
			//       	Mage::log($dataItem);
			$orderNumber = $xmlDataArrayHeader['Reference1'];
			$trackingNumber = $xmlDataArrayHeader['ConsignmentNumber'];
			$carrierName = $xmlDataArrayHeader['CourierReference1'];
			$sku = $dataItem['Product'];
			$qty = $dataItem['Quantity'];

			$shippedOrders[$orderNumber][$trackingNumber]['carrier'] = $carrierName;
			$shippedOrders[$orderNumber][$trackingNumber]['items'][$sku] = $qty;


		}
		//        Mage::log($shippedOrders);

		foreach($shippedOrders as $shippedOrderNumber => $shippedOrderTracking) {

			try {
				$order = Mage::getModel('sales/order')->loadByIncrementId($shippedOrderNumber);
			}
			catch (Exception $ex)
			{
				Mage::log($ex->getMessage(), null, 'tfsdispatch.log');
				continue;
			}
			$orderStatus = $order->getStatus();
			//            Mage::log($orderStatus);
			// A J Monaghan
			// 8th April 2014
			// Unsure of why this is checking as it is the return journey so it has shipped!
			//			if($order->canShip()) {

			foreach($shippedOrderTracking as $shippedOrderTrackingNumber => $shippedOrderDetails)
			{
				try
				{
					$convertOrder = new Mage_Sales_Model_Convert_Order();
					$shipment = $convertOrder->toShipment($order);

					$items = $order->getAllItems();
					//                    Mage::log($items);
					$totalQty = 0;
					if (count($items)) {
						foreach ($items as $_eachItem) {
							$_eachSku = $_eachItem->getSku();
							//                            Mage::log($_eachSku);
							if(isset($shippedOrderDetails['items'][$_eachSku])) {

								$_eachShippedItem = $convertOrder->itemToShipmentItem($_eachItem);
								$_eachShippedItem->setQty($shippedOrderDetails['items'][$_eachSku]);
								$shipment->addItem($_eachShippedItem);
								$totalQty += $shippedOrderDetails['items'][$_eachSku];
								unset($_eachShippedItem);
							}
						}

						$shipment->setTotalQty($totalQty);
					}

					// Issues due to text being non-unique, requested to send just the number from Optima
					//$TrackingNumber = 'No Tracking available on this shipment';
					//if (substr($shippedOrderTrackingNumber, 0 , 3) != 'PAP')
					//{
					$TrackingNumber = $shippedOrderTrackingNumber;
					//}
					$trackingData = array(
							'carrier_code' => $carrierName,
							'title' => $carrierName,
							'number' => $TrackingNumber,
					);
					//                    Mage::log($trackingData);
					$track = Mage::getModel('sales/order_shipment_track')->addData($trackingData);
					$shipment->addTrack($track);

					$shipment->register();

					$emailSentStatus = $shipment->getData('email_sent');
					$customerEmail = $order->getData('customer_email');
					$customerEmailComments = 'The order has been dispatched';
					if (!is_null($customerEmail) && !$emailSentStatus) {
						$shipment->sendEmail($customerEmail, $customerEmailComments);
						$shipment->setEmailSent(true);
					}
					//Mage::log($shipment);
					$saveTransaction = Mage::getModel('core/resource_transaction')
					->addObject($shipment)
					//				->addObject($shipment->getOrder())
					->addObject($order)
					->save();

					# todo:
					# $status = $totalQty == $shippedTotalQty ? 'shipped' : 'partial';
					//                    $status = 'shipped';
					//                    $order->setState(Mage_Sales_Model_Order::STATE_COMPLETE, true, 'The order has been dispatched', true );
					//				$order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
					//                    $order->addStatusToHistory($status, $status, true);
					$order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
					$order->setData('status', Mage_Sales_Model_Order::STATE_COMPLETE);

					$order->save();

					unset($orderStatus, $convertOrder, $shipment, $items, $trackingData);
					unset($track, $emailSentStatus, $customerEmail, $saveTransaction);
					unset($totalQty);
				}
					
				catch (Exception $ex)
				{
					Mage::log($ex->getMessage(), null, 'tfsdispatch.log');
					//Mage::log($xmlDataArray, null, 'tfsdispatch.log');
					Mage::log($shippedOrders, null, 'tfsdispatch.log');
					Mage::log("$shippedOrderNumber: $orderStatus", null, 'tfsdispatch.log');
					continue;
				}
			}
			//			} else {
			//throw new Exception("This order cannot be shipped at this time.");
			//				Mage::log("This order cannot be shipped at this time.");
			//	}
			unset($order);

		}
		return true;

	}

	/**
	 * xml2array() will convert the given XML text to an array in the XML structure.
	 * Link: http://www.bin-co.com/php/scripts/xml2array/
	 * Arguments : $contents - The XML text
	 *                $get_attributes - 1 or 0. If this is 1 the function will get the attributes as well as the tag values - this results in a different array structure in the return value.
	 *                $priority - Can be 'tag' or 'attribute'. This will change the way the resulting array sturcture. For 'tag', the tags are given more importance.
	 * Return: The parsed XML in an array form. Use print_r() to see the resulting array structure.
	 * Examples: $array =  xml2array(file_get_contents('feed.xml'));
	 *              $array =  xml2array(file_get_contents('feed.xml', 1, 'attribute'));
	 */
	public function xml2array($contents, $get_attributes = 0, $priority = 'tag') {
		if(!$contents) return array();

		if(!function_exists('xml_parser_create')) {
			//print "'xml_parser_create()' function not found!";
			return array();
		}

		//Get the XML parser of PHP - PHP must have this module for the parser to work
		$parser = xml_parser_create('');
		xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
		xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
		xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
		xml_parse_into_struct($parser, trim($contents), $xml_values);
		xml_parser_free($parser);

		if(!$xml_values) return;//Hmm...

		//Initializations
		$xml_array = array();
		$parents = array();
		$opened_tags = array();
		$arr = array();

		$current = &$xml_array; //Refference

		//Go through the tags.
		$repeated_tag_index = array();//Multiple tags with same name will be turned into an array
		foreach($xml_values as $data) {
			unset($attributes,$value);//Remove existing values, or there will be trouble

			//This command will extract these variables into the foreach scope
			// tag(string), type(string), level(int), attributes(array).
			extract($data);//We could use the array by itself, but this cooler.

			$result = array();
			$attributes_data = array();

			if(isset($value)) {
				if($priority == 'tag') $result = $value;
				else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
			}

			//Set the attributes too.
			if(isset($attributes) and $get_attributes) {
				foreach($attributes as $attr => $val) {
					if($priority == 'tag') $attributes_data[$attr] = $val;
					else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
				}
			}

			//See tag status and do the needed.
			if($type == "open") {//The starting of the tag '<tag>'
				$parent[$level-1] = &$current;
				if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
					$current[$tag] = $result;
					if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
					$repeated_tag_index[$tag.'_'.$level] = 1;

					$current = &$current[$tag];

				} else { //There was another element with the same tag name

					if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
						$repeated_tag_index[$tag.'_'.$level]++;
					} else {//This section will make the value an array if multiple tags with the same name appear together
						$current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
						$repeated_tag_index[$tag.'_'.$level] = 2;

						if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
							$current[$tag]['0_attr'] = $current[$tag.'_attr'];
							unset($current[$tag.'_attr']);
						}

					}
					$last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
					$current = &$current[$tag][$last_item_index];
				}

			} elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
				//See if the key is already taken.
				if(!isset($current[$tag])) { //New Key
					$current[$tag] = $result;
					$repeated_tag_index[$tag.'_'.$level] = 1;
					if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;

				} else { //If taken, put all things inside a list(array)
					if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...

						// ...push the new element into that array.
						$current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;

						if($priority == 'tag' and $get_attributes and $attributes_data) {
							$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
						}
						$repeated_tag_index[$tag.'_'.$level]++;

					} else { //If it is not an array...
						$current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
						$repeated_tag_index[$tag.'_'.$level] = 1;
						if($priority == 'tag' and $get_attributes) {
							if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well

								$current[$tag]['0_attr'] = $current[$tag.'_attr'];
								unset($current[$tag.'_attr']);
							}

							if($attributes_data) {
								$current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
							}
						}
						$repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
					}
				}

			} elseif($type == 'close') { //End of tag '</tag>'
				$current = &$parent[$level-1];
			}
		}

		return($xml_array);
	}

	public function stocklevel()
	{
		$xmlDataArray = $this->xml2array($this->getData());
		//    	Mage::log($xmlDataArray);
		$headKey = isset($xmlDataArray['Root']) ? 'Root' : (isset($xmlDataArray['root']) ? 'root' :  'ROOT');
		if(!isset($xmlDataArray[$headKey])) throw new Exception("Invalid file format!");


		foreach ($xmlDataArray[$headKey]['Stock'] as $key=>$stock)
		{
			// 			Mage::log("Stock Key: $key");
			// 			Mage::log($stock);
			try
			{
				$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $stock['Code']);
				// 				Mage::log('Product :');
				// 				Mage::log($product);
			}
			catch (Exception $ex)
			{
				Mage::log($ex->getMessage());
			}
			if ($product)
			{
				Mage::log('Product Id : '. $product->getId());
				if ($product->getId())
				{
					$stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
					Mage::log('SKU: ' . $stock['Code'] . ' Qty: '. $stock['SaleQty']);
					//    		Mage::log($product);
					//    		Mage::log($stock_item);
					$stock_item->setData('qty', $stock['SaleQty']);
					if ($stock['SaleQty'])
					{
						$instock = 1;
					}
					else
					{
						$instock = 0;
					}
					$stock_item->setData('is_in_stock', $instock);
					$stock_item->save();
				}
			}
			else
			{
				Mage::log('SKU:' .$stock['Code'] . ' not found' );
			}
		}
		return true;
	}


}