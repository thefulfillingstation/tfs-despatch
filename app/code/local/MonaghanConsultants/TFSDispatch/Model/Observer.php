<?php

/*
 * Code lifted from "LaunchPad DispatchMan" as supplied by TFS Code debugged to remove long list of issues with processing XML data A J Monaghan Monaghan Consultants Ltd 14th October 2013
 */
class MonaghanConsultants_TFSDispatch_Model_Observer extends Mage_Core_Model_Abstract {
	public function dispatch2($observer) {
		Mage::Log ( "Dispatch 2", null, 'tfsdispatch.log' );
		Mage::Log ( "Trigger: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger' ), null, 'tfsdispatch.log' );
		// if (Mage::getStoreConfig('tfsdispatch/tfsdispatch/pay_trigger'))
		// {
		// // Mage::log($observer->getEvent()->getControllerAction()->getFullActionName());
		// Mage::Log("Dispatch 2");
		// // Mage::Log(Mage::getStoreConfig('tfsdispatch/tfsdispatch/pay_trigger'));
		// // Mage::Log(Mage::getStoreConfig('tfsdispatch/tfsdispatch/pay_trigger_status'));
		
		// try
		// { // If we don't have the order / invoice then we're not through the process far enough to worry.
		
		// // $invoice = $observer->getEvent()->getInvoice();
		// $payment = $observer->getEvent()->getPayment();
		// $order = $payment->getOrder();
		// // $order = $observer->getOrder();
		// // if (method_exists($invoice, "getOrder"))
		// // {
		// // $order = $invoice->getOrder();
		// Mage::Log("Order Status: " . $order->getStatus());
		// Mage::Log("Order State: " . $order->getState());
		// Mage::Log("looking for: " .Mage::getStoreConfig('tfsdispatch/tfsdispatch/pay_trigger_status'));
		// if ($order->getStatus() == Mage::getStoreConfig('tfsdispatch/tfsdispatch/pay_trigger_status')) // Need to check status
		// {
		// Mage::Log("Status Match - Sending to TFS");
		// $this->sendtotfs($order);
		// }
		// else
		// {
		// Mage::Log("Status not for dispatch");
		// Mage::Log("Order Status: " . $order->getStatus());
		// Mage::Log("Order State: " . $order->getState());
		// Mage::Log("looking for: " .Mage::getStoreConfig('tfsdispatch/tfsdispatch/pay_trigger_status'));
		
		// }
		// // }
		// // else
		// // {
		// // Mage::Log("Status not for dispatch - no invoice yet");
		
		// // }
		// }
		// catch (Exception $ex)
		// {
		// Mage::log("Exception: " . $ex->getMessage());
		// }
		// }
	}
	public function dispatch($observer) {
		// Mage::log($observer->getEvent()->getControllerAction()->getFullActionName());
		// return;
		Mage::Log ( "Dispatch", null, 'tfsdispatch.log' );
		Mage::Log ( "Pay Trigger: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger' ), null, 'tfsdispatch.log' );
		Mage::Log ( "Pay Trigger Status: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger_status' ), null, 'tfsdispatch.log' );
		
		if (Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger' ) == 1) 		// If Yes
		{
			$invoice = $observer->getEvent ()->getInvoice ();
			$order = $invoice->getOrder ();
			Mage::Log ( "Order State: " . $order->getState (), null, 'tfsdispatch.log' );
			Mage::Log ( "Order Status: " . $order->getStatus (), null, 'tfsdispatch.log' );
			
			$this->sendtotfs ( $order );
		} else {
			Mage::log ( "Selective Dispatch selected, not processing this event", null, 'tfsdispatch.log' );
		}
	}
	public function dispatch3($observer) {
		Mage::Log ( "Dispatch 3", null, 'tfsdispatch.log' );
		Mage::Log ( "Pay Trigger: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger' ), null, 'tfsdispatch.log' );
		Mage::Log ( "Status Trigger: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/any_trigger' ), null, 'tfsdispatch.log' );
		if ((Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger' ) == 1) or (Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/any_trigger' ) == 1)) {
			// Mage::log($observer->getEvent()->getControllerAction()->getFullActionName());
			Mage::Log ( "Dispatch 3 - Are we needed?", null, 'tfsdispatch.log' );
			
			$status = $observer->getEvent ()->getOrder ()->getStatus ();
			$originalData = $observer->getEvent ()->getOrder ()->getOrigData ();
			$previousStatus = $originalData ['status'];
			
			if ($status !== $previousStatus) {
				// A J Monaghan 12th August 2014
				// Convert the any trigger status to an array and check individual values
				// The stripos functions may find a substring within another value and create a false trigger
				$any_status = explode ( ',', Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/any_trigger_status' ) );
				Mage::Log ( "Any Status: " . print_r ( $any_status, true ), null, 'tfsdispatch.log' );
				
				if (((Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger' ) == 1) and ($status == Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger_status' ))) or ((Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/any_trigger' ) == 1) and (in_array ( $status, $any_status ) !== false))
				)
				//do something when the order changes to our desired status 
				{

					try {
						$order = $observer->getEvent ()->getOrder ();
						Mage::log ( "Previous Order Status: $previousStatus", null, 'tfsdispatch.log' );
						Mage::Log ( "Order Status: " . $order->getStatus (), null, 'tfsdispatch.log' );
						Mage::Log ( "Order State: " . $order->getState (), null, 'tfsdispatch.log' );
						Mage::Log ( "looking for pay trigger: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger_status' ), null, 'tfsdispatch.log' );
						Mage::Log ( "looking for any trigger: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/any_trigger_status' ), null, 'tfsdispatch.log' );
						Mage::Log ( "Status Match - Sending to TFS", null, 'tfsdispatch.log' );
						// Mage::Log("Order Object: " . print_r($order->getData(), true));
						$this->sendtotfs ( $order );
					} catch ( Exception $ex ) {
						Mage::log ( "Exception: " . $ex->getMessage (), null, 'tfsdispatch.log' );
					}
				} else {
					Mage::Log ( "Status not for dispatch", null, 'tfsdispatch.log' );
					Mage::log ( "Previous Order Status: $previousStatus", null, 'tfsdispatch.log' );
					Mage::Log ( "Order Status: " . $status, null, 'tfsdispatch.log' );
					Mage::Log ( "looking for: " . Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/pay_trigger_status' ), null, 'tfsdispatch.log' );
				}
			}
		}
	}
	function sendtotfs($order) {
		if (Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/send_enabled' ) == 1) {
			try {
				// build items
				
				$items = $order->getAllItems ();
				// $items = $order->getAllVisibleItems();
				// Mage::log($items);
				$itemcount = count ( $items );
				$quoteitems = array ();
				foreach ( $items as $itemId => $item ) {
					// if we have no children then we've decended into the bundle or combination
					// so we're at the SKU to send for dispatch
					if (count ( $item->getChildrenItems () ) == 0) {
						array_push ( $quoteitems, array (
								'Quantity' => $item->getQtyOrdered (),
								'ProductCode' => $item->getSku () 
						) );
					}
				}
				// Mage::log($quoteitems);
				// build header
				$customer = Mage::getModel ( 'customer/customer' )->load ( $order->getCustomerId () );
				$shippingAddress = $order->getShippingAddress ();
				// A J Monaghan 8th October 2013
				// Check for 2nd line of the address, if not supplied insert town as Optima expects 2 lines of address
				if ($shippingAddress->getStreet ( 2 )) {
					$addr2 = $shippingAddress->getStreet ( 2 );
				} else {
					$addr2 = $shippingAddress->getCity ();
				}
				
				// A J Monaghan 12th October 2015
				// Check the shipping address for a company and contact and apply accordingly,
				// if not default to looking at the customer and order data
				
				if ($shippingAddress->getCompany () != "") {
					$customer_name = $shippingAddress->getCompany ();
					$customer_contact = $shippingAddress->getFirstname () . " " . $shippingAddress->getLastname ();
				} else {
					if (ltrim ( $customer->getName () ) != "") {
						$customer_name = $customer->getName ();
					} else {
						$customer_name = $order->getData ( "customer_firstname" ) . " " . $order->getData ( "customer_lastname" );
					}
				}
				if ($customer->getEmail ()) {
					$customer_email = $customer->getEmail ();
				}
				{
					$customer_email = $order->getData ( "customer_email" );
				}
				$writerArray = array (
						'H' => array (
								'Reference1' => "{$order->getIncrementId()}",
								'DeliveryName' => $customer_name,
								'DeliveryContact' => $customer_contact,
								'DeliveryAddress1' => $shippingAddress->getStreet ( 1 ),
								'DeliveryAddress2' => $addr2,
								'DeliveryTown' => $shippingAddress->getCity (),
								'DeliveryPostCode' => $shippingAddress->getPostcode (),
								'DeliveryCountry' => Mage::app ()->getLocale ()->getCountryTranslation ( $shippingAddress->getCountry () ),
								'DeliveryEmail' => $customer_email,
								'DeliveryTelephone' => $shippingAddress->getTelephone (),
								'DeliveryService' => $order->getData ( "shipping_method" ) 
						),
						'D' => $quoteitems 
				);
				// A J Monaghan 12th Aug 2014
				// Added check for an additional "delivery_request" value in the order,
				// if present, convert to DDMMYYY and add send as the Header "DeliveryDate" value
				try {
					$delivery_date = $order->getData ( 'delivery_request' );
					// Mage::log("Delivery Date: $delivery_date ", null, 'tfsdispatch.log');
				} catch ( Exception $ex ) {
					Mage::log ( "Exception: " . $ex->getMessage (), null, 'tfsdispatch.log' );
				}
				if (! empty ( $delivery_date )) {
					$writerArray ['H'] ['DeliveryDate'] = date ( 'dmY', strtotime ( str_replace ( '/', '-', $delivery_date ) ) );
				}
				try {
					$delivery_instructions = $order->getData ( 'delivery_instructions' );
				} catch ( Exception $ex ) {
					Mage::log ( "Exception: " . $ex->getMessage (), null, 'tfsdispatch.log' );
				}
				if (! empty ( $delivery_instructions )) {
					$writerArray ['H'] ['DeliveryInstructions'] = substr ( $delivery_instructions, 0, 250 );
				}
				
				// name file, archive and send
				
				$timeOfImport = date ( 'jmY_his' );
				$filename = "{$timeOfImport}_{$order->getIncrementId()}.xml";
				$xmlWriter = new MonaghanConsultants_TFSDispatch_Helper_Writer ();
				$xmlWriter->setData ( $writerArray )->writeConfig ( $filename );
				
				// keep track
				Mage::log ( 'Filename ' . $filename, null, 'tfsdispatch.log' );
				// Mage::log(print_r($writerArray, true));
			} catch ( Exception $ex ) {
				Mage::log ( "Exception: " . $ex->getMessage (), null, 'tfsdispatch.log' );
			}
		}
	}
	public function scheduledPollOptimiserOut(Mage_Cron_Model_Schedule $schedule) {
		// Mage::log("Cron fired");
		if ((Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/process_shipping_notifications' ) == 1) or (Mage::getStoreConfig ( 'tfsdispatch/tfsdispatch/process_stock_notifications' ) == 1)) {
			$reader = new MonaghanConsultants_TFSDispatch_Helper_Reader ();
			$reader->readConfig ();
		}
	}
}